package com.ks.exception;

/**
 * 自定义异常，抽象类
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/24
 */
public abstract class BaseException extends Exception {
    public BaseException(String message) {
        super(message);
    }
}
