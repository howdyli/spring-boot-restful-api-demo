package com.ks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.*;
import static com.google.common.base.Predicates.*;
import com.google.common.base.Predicate;


@Configuration
@SpringBootApplication
@EnableSwagger2
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public Docket restApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("auth")
//                .genericModelSubstitutes(CityController.class)
                //.genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .pathMapping("/")//api测试请求地址
                .select()
                .paths(paths())//过滤的接口
                .build()
                .apiInfo(apiInfo());
    }

    private Predicate<String> paths() {
        return or(
                regex("/echo"),
                regex("/cities.*"),
                regex("/roles.*"),
                regex("/users.*"),
                regex("/auth.*"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Boot RESTFul Demo ")
                .description("spring data jpa + spring security + mysql")
                .termsOfServiceUrl("http://localhost:8088")
                .contact(new Contact("kuShao", "", "kushaobuy@163.com"))
//                .contact("kushao <kushaobuy@163.com>")
                .version("1.0")
                .build();
    }
}
