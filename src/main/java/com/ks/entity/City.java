package com.ks.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/25
 */

@Entity
@Table(name = "city")
//@PrimaryKeyJoinColumn(name = "ID")
public class City {
    public City() {
        super();
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "Name", length = 35)
    @NotNull(message="不能为空")
    private String name;

    @NotNull(message="不能为空")
    @Column(name = "CountryCode", length = 3)
    @Size(min = 3, max = 3, message = "最大3位")
    private String countryCode;

    @NotNull(message="不能为空")
    @Column(name = "District", length = 20)
    private String district;

    @NotNull(message="不能为空")
    @Column(name = "Population")
    private int population;

    public Long getId() {
        return id;
    }
    @ApiModelProperty(hidden = true)
    public void setId(Long id){
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getDistrict(){
        return district;
    }
    public void setDistrict(String district){
        this.district = district;
    }

    public int getPopulation(){
        return population;
    }
    public void setPopulation(int population){
        this.population = population;
    }

    public String getCountryCode() {
        return this.countryCode;
    }
    public void setCountryCode(String countryCode){
        this.countryCode = countryCode;
    }
}
