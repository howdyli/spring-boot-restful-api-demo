package com.ks.entity;

/**
 *
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/25
 */
public class Message {
    private boolean isSuccess;

    private String message;

    private Object entity;

    public Message(){
        super();
    }

    public Message(boolean isSuccess, String message, Object entity){
        this.isSuccess = isSuccess;
        this.message = message;
        this.entity = entity;
    }

    public Message(boolean isSuccess, String message){
        this.isSuccess = isSuccess;
        this.message = message;
        this.entity = null;
    }

    public boolean getSuccess(){
        return this.isSuccess;
    }

    public void setSuccess(boolean isSuccess){
        this.isSuccess = isSuccess;
    }

    public String getMessage(){
        return this.message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public Object getEntity(){
        return this.entity;
    }

    public void setEntity(Object entity){
        this.entity = entity;
    }

}
