package com.ks.service;

import com.ks.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.ks.repository.SysUserRepository;
import com.ks.entity.SysUser;
import org.springframework.stereotype.Service;

/**
 * 自定义userDetailService
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/27
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    SysUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UserNotFoundException {
        SysUser user = userRepository.findByUsername(userName);

        if (user == null) {
            throw new UserNotFoundException("用户名不存在");
        }else{
            return user;
        }

    }

}
