package com.ks.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import com.ks.repository.SysRoleRepository;
import com.ks.entity.SysRole;
import com.ks.utils.Result;

/**
 * 角色控制器
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/27
 */
@RestController
@RequestMapping("/roles")
@Api(tags = "Role", value = "Role", description = "角色")
public class RoleController {

    @Autowired
    private SysRoleRepository sysRoleRepository;

    @ApiOperation(value = "新建角色")
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysRole", value = "角色", required = true, paramType = "body", dataType = "SysRole")
    })
    public Result addRole(@RequestBody @Valid SysRole sysRole) {

        this.sysRoleRepository.save(sysRole);

        return Result.fillResult(true, "成功！", sysRole);
    }

}
