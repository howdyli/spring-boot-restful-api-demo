package com.ks.controller;

import com.ks.utils.Result;
import com.ks.entity.SysUser;
import com.ks.repository.SysUserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import javax.validation.Valid;

/**
 * 用户控制器
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/27
 */
@RestController
@RequestMapping("/users")
@Api(tags = "User", value = "User", description = "用户")
public class UserController {
    @Autowired
    private SysUserRepository sysUserRepository;

    @ApiOperation(value = "新建用户")
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户", required = true, paramType = "body", dataType = "SysUser")
    })
    public Result addRole(@RequestBody @Valid SysUser sysUser) {

        BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
        String password = sysUser.getPassword();
        sysUser.setPassword(bCrypt.encode(password));
        this.sysUserRepository.save(sysUser);

        return Result.fillResult(true, "成功！", sysUser);
    }
}
