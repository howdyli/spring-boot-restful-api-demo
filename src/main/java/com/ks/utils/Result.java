package com.ks.utils;

import io.swagger.annotations.ApiModelProperty;

/**
 * 结果对象
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/28
 */
public class Result {
    public static Result fillResult(boolean isSuccess , String message, Object result){
        return new Result(isSuccess, message, result);
    }

    public Result(boolean isSuccess , String message, Object result){
        this.isSuccess = isSuccess;
        this.message = message;
        this.result = result;
    }

    private boolean isSuccess = true;
    private String message = "成功";
    private Object result = null;

    @ApiModelProperty(value = "操作是否成功")
    public Boolean getIsSuccess() {
        return isSuccess;
    }

    @ApiModelProperty(value = "反馈消息")
    public String getMessage() {
        return message;
    }

    @ApiModelProperty(value = "结果")
    public Object getResult() {
        return result;
    }
}
